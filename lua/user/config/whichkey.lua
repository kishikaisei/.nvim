local status_ok, which_key = pcall(require, "which-key")
if not status_ok then
  return
end

local setup = {
  plugins = {
    marks = true, -- shows a list of your marks on " and `
    registers = true, -- shows your registers on " in NORMAL or <C-r> in INSERT mode
    spelling = {
      enabled = true, -- enabling this will show WhichKey when pressing z= to select spelling suggestions
      suggestions = 20, -- how many suggestions should be shown in the list?
    },
    -- the presets plugin, adds help for a bunch of default keybindings in Neovim
    -- No actual key bindings are created
    presets = {
      operators = false, -- adds help for operators like d, y, ... and registers them for motion / text object completion
      motions = false, -- adds help for motions
      text_objects = false, -- help for text objects triggered after entering an operator
      windows = true, -- default bindings on <c-w>
      nav = true, -- misc bindings to work with windows
      z = true, -- bindings for folds, spelling and others prefixed with z
      g = true, -- bindings for prefixed with g
    },
  },
  -- add operators that will trigger motion and text object completion
  -- to enable all native operators, set the preset / operators plugin above
  -- operators = { gc = "Comments" },
  key_labels = {
    -- override the label used to display some keys. It doesn"t effect WK in any other way.
    -- For example:
    -- ["<space>"] = "SPC",
    -- ["<cr>"] = "RET",
    -- ["<tab>"] = "TAB",
  },
  icons = {
    breadcrumb = "»", -- symbol used in the command line area that shows your active key combo
    separator = "ﰲ", -- symbol used between a key and it"s label
    group = " ", -- symbol prepended to a group
  },
  popup_mappings = {
    scroll_down = "<c-d>", -- binding to scroll down inside the popup
    scroll_up = "<c-u>", -- binding to scroll up inside the popup
  },
  window = {
    border = "rounded", -- none, single, double, shadow
    position = "bottom", -- bottom, top
    margin = { 1, 0, 1, 0 }, -- extra window margin [top, right, bottom, left]
    padding = { 2, 2, 2, 2 }, -- extra window padding [top, right, bottom, left]
    winblend = 0,
  },
  layout = {
    height = { min = 4, max = 25 }, -- min and max height of the columns
    width = { min = 20, max = 50 }, -- min and max width of the columns
    spacing = 3, -- spacing between columns
    align = "center", -- align columns left, center or right
  },
  ignore_missing = true, -- enable this to hide mappings for which you didn"t specify a label
  hidden = { "<silent>", "<cmd>", "<Cmd>", "<CR>", "call", "lua", "^:", "^ " }, -- hide mapping boilerplate
  show_help = true, -- show help message on the command line when the popup is visible
  triggers = "auto", -- automatically setup triggers
  -- triggers = {"<leader>"} -- or specify a list manually
  triggers_blacklist = {
    -- list of mode / prefixes that should never be hooked by WhichKey
    -- this is mostly relevant for key maps that start with a native binding
    -- most people should not need to change this
    i = { "j", "k" },
    v = { "j", "k" },
  },
}

local opts = {
  mode = "n", -- NORMAL mode
  prefix = "<leader>",
  buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
  silent = true, -- use `silent` when creating keymaps
  noremap = true, -- use `noremap` when creating keymaps
  nowait = true, -- use `nowait` when creating keymaps
}

local vopts = {
  mode = "v", -- NORMAL mode
  prefix = "<leader>",
  buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
  silent = true, -- use `silent` when creating keymaps
  noremap = true, -- use `noremap` when creating keymaps
  nowait = true, -- use `nowait` when creating keymaps
}

local mappings = {
  ["/"] = { "<cmd>CommentToggle<cr>" , "Comment"},
  ["B"] = { "<cmd>Neotree toggle buffers<cr>" , "Buffer tree"},
  ["H"] = { "<C-W>H" , "Split to vertical"},
  ["V"] = { "<C-W>K" , "Split to horizontal"},
  ["a"] = { "<cmd>Alpha<cr>", "Alpha" },
  ["b"] = {
    "<cmd>lua require('telescope.builtin').buffers(require('telescope.themes').get_dropdown{previewer = false})<cr>",
    "Buffers",
  },
  ["c"] = { "<cmd>Bdelete!<CR>", "Close Buffer" },
  ["e"] = { "<cmd>Neotree toggle left reveal<cr>", "Explorer" },
  -- ["e"] = { "<cmd>NvimTreeFindFileToggle<cr>", "Explorer" },
  ["h"] = { "<C-W>s" , "Split below"},
  ["m"] = { "<cmd>NeoZoomToggle<CR>" , "Maximise window"},
  ["q"] = { "<cmd>q!<CR>", "Quit" },
  ["r"] = { ":retab<CR>=%" , "Retab"},
  -- ["r"] = { ":e! %<CR>" , "Reload file"},
  ["u"] = { ":MundoToggle<CR>" , "Undo tree"},
  ["v"] = { "<C-W>v" , "Split right"},
  ["z"] = { "<cmd>ZenMode<cr>", "Zen" },

  f = {
    name = "Find",
    ["?"] = { "<cmd>Telescope help_tags<cr>", "Find Help" },
		["B"] = { "<cmd>lua require('telescope.builtin').buffers()<CR>", "Find Buffer"},
    ["C"] = { "<cmd>Telescope commands<cr>", "Commands" },
    ["D"] = { "<cmd>Telescope lsp_workspace_diagnostics()<CR>", "Workspace diagnostics"},
    ["M"] = { "<cmd>Telescope man_pages<cr>", "Man Pages" }, ----
    ["R"] = { "<cmd>Telescope registers<cr>", "Registers" },
    ["b"] = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
    ["c"] = { "<cmd>Telescope colorscheme<cr>", "Colorscheme" },
    ["d"] = { "<cmd>Telescope lsp_document_diagnostics()<CR>", "Document diagnostics"},
    ["f"] = { "<cmd>lua require('telescope.builtin').find_files(require('telescope.themes').get_dropdown{previewer = false})<cr>", "Find files" },
    ["h"] = { "<cmd>Telescope oldfiles<cr>", "Open Recent File" },
    ["k"] = { "<cmd>Telescope keymaps<cr>", "Keymaps" }, ----
    ["m"] = { "<cmd>Telescope marks()<CR>", "Find Marks"},
    ["p"] = { "<cmd>Telescope projects<cr>", "Projects" },
    ["s"] = { "<cmd>lua require('telescope').extensions.packagescript.scripts()<CR>", "package scripts"},
    ["t"] = { "<cmd>TodoTelescope<CR>", "TODOs"},
    ["w"] = { "<cmd>Telescope live_grep theme=ivy<cr>", "Find Text" },
  },

  g = {
    name = "Git",
    ["R"] = { "<cmd>lua require 'gitsigns'.reset_buffer()<cr>", "Reset Buffer" },
    ["b"] = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
    ["c"] = { "<cmd>Telescope git_commits<cr>", "Checkout commit" },
    ["d"] = { "<cmd>Gitsigns diffthis HEAD<cr>", "Diff" },
    ["g"] = { "<cmd>lua _LAZYGIT_TOGGLE()<CR>", "Lazygit" },
    ["j"] = { "<cmd>lua require'gitsigns'.next_hunk()<cr>", "Next Hunk" },
    ["k"] = { "<cmd>lua require'gitsigns'.prev_hunk()<cr>", "Prev Hunk" },
    ["l"] = { "<cmd>lua require'gitsigns'.blame_line()<cr>", "Blame" },
    ["o"] = { "<cmd>Telescope git_status<cr>", "Open changed file" },
    ["p"] = { "<cmd>lua require'gitsigns'.preview_hunk()<cr>", "Preview Hunk" },
    ["r"] = { "<cmd>lua require'gitsigns'.reset_hunk()<cr>", "Reset Hunk" },
    ["s"] = { "<cmd>lua require'gitsigns'.stage_hunk()<cr>", "Stage Hunk" },
    ["u"] = { "<cmd>lua require'gitsigns'.undo_stage_hunk()<cr>", "Undo Stage Hunk" },
  },

  l = {
    name = "LSP",
  	["D"] = { "<cmd>Telescope lsp_workspace_diagnostics<CR>", "workspace diagnostics"},
    ["I"] = { "<cmd>LspInstallInfo<cr>", "Installer Info" },
    ["S"] = { "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>", "Workspace Symbols" },
    ["a"] = { "<cmd>lua vim.lsp.buf.code_action()<cr>", "Code Action" },
    ["d"] = { "<cmd>Telescope lsp_document_diagnostics<cr>", "Document Diagnostics" },
    ["f"] = { "<cmd>lua vim.lsp.buf.formatting()<cr>", "Format" },
    ["i"] = { "<cmd>LspInfo<cr>", "Info" },
    ["j"] = { "<cmd>lua vim.diagnostic.goto_next()<CR>", "Next Diagnostic" },
    ["k"] = { "<cmd>lua vim.diagnostic.goto_prev()<cr>", "Prev Diagnostic" },
    ["l"] = { "<cmd>lua vim.lsp.codelens.run()<cr>", "CodeLens Action" },
    ["o"] = { "<cmd>SymbolsOutline<cr>", "Outline" },
    ["q"] = { "<cmd>lua vim.diagnostic.set_loclist()<cr>", "Quickfix" },
    ["r"] = { "<cmd>lua vim.lsp.buf.rename()<cr>", "Rename" },
    ["s"] = { "<cmd>Telescope lsp_document_symbols<cr>", "Document Symbols" },
    ["t"] = { "<cmd>require('lsp_lines').toggle<cr>", "Toggle LSP lines" },
    ["w"] = { "<cmd>Telescope lsp_workspace_diagnostics<cr>", "Workspace Diagnostics" },

    -- buf_inoremap { "<c-s>", vim.lsp.buf.signature_help }
      
    -- buf_nnoremap { "gd", vim.lsp.buf.definition }
    -- buf_nnoremap { "gD", vim.lsp.buf.declaration }
    -- buf_nnoremap { "gT", vim.lsp.buf.type_definition }

    -- buf_nnoremap { "<space>gI", handlers.implementation }
    -- buf_nnoremap { "<space>lr", "<cmd>lua R("tj.lsp.codelens").run()<CR>" }
    -- buf_nnoremap { "<space>rr", "LspRestart" }

    -- telescope_mapper("gr", "lsp_references", nil, true)
    -- telescope_mapper("gI", "lsp_implementations", nil, true)
    -- telescope_mapper("<space>wd", "lsp_document_symbols", { ignore_filename = true }, true)
    -- telescope_mapper("<space>ww", "lsp_dynamic_workspace_symbols", { ignore_filename = true }, true)

  	-- A = { "<cmd>Lspsaga range_code_action<CR>", "selected action"},
  	-- K = { "<cmd>Lspsaga hover_doc<cr>", "Hover doc"},
  	-- L = { "<cmd>Lspsaga show_line_diagnostics<CR>", "line_diagnostics"},
  	-- T = { "<cmd>LspTypeDefinition<CR>", "type defintion"},          -- not working
  	-- a = { "<cmd>Lspsaga code_action<CR>", "code action"},
  	-- f = { "<cmd>LspFormatting<CR>", "format"},                  -- not working
  	-- l = { "<cmd>Lspsaga lsp_finder<CR>", "lsp finder"},
  	-- p = { "<cmd>Lspsaga preview_definition<CR>", "preview definition"},      -- not working
  	-- q = { "<cmd>Telescope quickfix<CR>", "quickfix"},                -- nothing happens
  	-- r = { "<cmd>Lspsaga rename<CR>", "rename"},
  	-- v = { "<cmd>LspVirtualTextToggle<CR>", "lsp toggle virtual text"}, -- not working
  	-- x = { "<cmd>cclose<CR>", "close quickfix"},
  },

  s = {
    name = "Replace",
    ["r"] = { "<cmd>lua require('spectre').open()<cr>", "Replace" },
    ["w"] = { "<cmd>lua require('spectre').open_visual({select_word=true})<cr>", "Replace Word" },
    ["f"] = { "<cmd>lua require('spectre').open_file_search()<cr>", "Replace Buffer" },
  },

  t = {
    name = "Terminal",
    ["b"] = { "<cmd>lua _BTM_TOGGLE()<cr>", "BTM" },
    ["g"] = { "<cmd>lua _LAZYGIT_TOGGLE()<cr>", "Lazygit" },
    ["s"] = { "<cmd>ToggleTerm size=10 direction=horizontal<cr>", "Horizontal" },
    ["t"] = { "<cmd>ToggleTerm direction=float<cr>", "Float" },
  },

	w = {
		["name"] = "Window",
		["="] = {"<C-W>=<CR>", "Balance windows"},
		["m"] = {":MaximizerToggle<CR>", "Maximise windows"},
		["s"] = {":call WindowSwap#EasyWindowSwap()<CR>", "Swap window" },
    ["l"] = {":BufferLineMoveNext<CR>", "Move right"},
    ["h"] = {":BufferLineMovePrev<CR>", "Move Left"},
    
    ["n"] = {":tabnew<CR>", "New tab"},
    ["L"] = {":tabnext<CR>", "Next tab"},
    ["H"] = {":tabnext<CR>", "Previous tab"},
    ["c"] = {":tabclose<CR>", "Close tab"},
	},
}

local vmappings = {
	["/"] = { ":CommentToggle<CR>", "Comment"},
	["s"] = { ":lua require('spectre').open_visual()<CR>", "Replace word"},
	["t"] = { ":Tabularize /", "Tabularize"},
}

which_key.setup(setup)
which_key.register(mappings, opts)
which_key.register(vmappings, vopts)
