local packer = require "user.packerInit"
return packer.startup(function(use)
  -- Dependencies --
  ------------------
  use {"wbthomason/packer.nvim"} -- Have packer manage itself
  use {"nvim-lua/plenary.nvim"} -- Useful lua functions used in lots of plugins
  use {"nvim-lua/popup.nvim"} -- An implementation of the Popup API from vim in Neovim
  use {"MunifTanjim/nui.nvim"}
  use {"tjdevries/colorbuddy.nvim"}

  --  Aesthetics --
  -------------------
	use {"luukvbaal/stabilize.nvim"} -- Stabilize view
	use {"norcalli/nvim-colorizer.lua"} -- Visualise colour codes
  use {"akinsho/bufferline.nvim", tag="v2.*"} -- Buffer tabs
  use {"folke/todo-comments.nvim"} -- Show to do list in qf
  use {"gelguy/wilder.nvim"} -- Better wildcard expansion
  use {"goolord/alpha-nvim"} -- Starting dashboard
  use {"habamax/vim-godot"} -- Godt syntax and scene runner
  use {"kishikaisei/bis-sqf"} -- BISim SQF syntax highlighting
  use {"kyazdani42/nvim-web-devicons"} -- Extra icons
  use {"lukas-reineke/indent-blankline.nvim"} -- Indent lines
  use {"booperlv/nvim-gomove"} -- Move text
  use {"nvim-lualine/lualine.nvim"} -- Status line
  use {"petertriho/nvim-scrollbar"} -- Scrollbar
  use {"karb94/neoscroll.nvim"} -- Smooth scroll
  use {"nyngwang/NeoZoom.lua"} -- Window maximiser
  use {"rcarriga/nvim-notify"} -- Nice notifications
  use {"folke/zen-mode.nvim"} -- Focus mode
  use {"folke/twilight.nvim"} -- Dim unfocused paragraphs
  use {"xiyaowong/nvim-cursorword"} -- Highlight current word
  use {"kevinhwang91/nvim-hlslens"} -- Better looking search
  use {"tiagovla/scope.nvim"} -- Only show buffers if current tab

  --  Tools --
  --------------
  use {"akinsho/toggleterm.nvim"} -- Terminal runner
  use {"voldikss/vim-floaterm"} -- Floating terminal
  use {"andymass/vim-matchup"} -- Better % movement
  use {"antoinemadec/FixCursorHold.nvim"} -- This is needed to fix lsp doc highlight
  use {"folke/trouble.nvim"} -- Better quickfix look
  use {"folke/which-key.nvim"} -- Emacs style whichkey
  use {"godlygeek/tabular"} -- Line up text based on character
  use {"jghauser/mkdir.nvim"} -- Automatically add the folders when saving
  use {"kyazdani42/nvim-tree.lua"} -- File explorer
  use {"nvim-neo-tree/neo-tree.nvim", branch = "v2.x"} -- File Explorer
  use {"lewis6991/impatient.nvim"} -- Speed-up loading plugins
  use {"max397574/better-escape.nvim"} -- Faster map for esc
  use {"moll/vim-bbye"} -- Better closing of buffers
  use {"terrortylor/nvim-comment"} -- Easily comment stuff
  use {"nvim-neorg/neorg"} -- Org-mode like note taking
  use {"rhysd/clever-f.vim"} -- Better F
  use {"simnalamburt/vim-mundo"} -- Visualise the undotree
  use {"windwp/nvim-autopairs"} -- Autopairs, integrates with both cmp and treesitter
  use {"kshenoy/vim-signature"} -- Show and navigate through marks
  use {"szw/vim-maximizer"} -- Toggle maximize panel
  use {"monaqa/dial.nvim"} -- More accurate increasing and decreasing
  use {"nacro90/numb.nvim"} -- Preview actions line number
  use {"windwp/nvim-ts-autotag"} -- Close tags
  use {"simrat39/symbols-outline.nvim"} -- Show symbols sidebar
  use {"nvim-pack/nvim-spectre"} -- Search and replace tool
  use {"s1n7ax/nvim-window-picker", tag = "v1.*"} -- Window picker
  use {"sbdchd/neoformat"} -- Formatting code
  use {"sindrets/diffview.nvim"} -- diffview
  use {"anuvyklack/pretty-fold.nvim"} -- Better looking folds
  use {"rmagatti/auto-session"} -- Session saver
  use {"kylechui/nvim-surround"} -- Edit surrounds

  -- ⚙ Code help --
  -----------------
  -- Completion
  use {"hrsh7th/nvim-cmp"} -- The completion plugin
  use {"hrsh7th/cmp-buffer"} -- Buffer completions
  use {"hrsh7th/cmp-path"} -- Path completions
  use {"hrsh7th/cmp-cmdline"} -- Cmdline completions
  use {"saadparwaiz1/cmp_luasnip"} -- Snippet completions
  use {"hrsh7th/cmp-nvim-lsp"} -- Neovim builtin LSP client
  use {"zbirenbaum/copilot-cmp", module = "copilot_cmp"} -- Github Copilot CMP
  -- Snippets
  use {"L3MON4D3/LuaSnip"} -- Snippet engine
  use {"rafamadriz/friendly-snippets"} -- A bunch of snippets to use
  -- LSP
  use {"neovim/nvim-lspconfig"} -- Enable LSP
  use {"williamboman/mason.nvim"} -- Simple to use language server installer
  use {"williamboman/mason-lspconfig.nvim"} -- LSPConfig for mason
  use {"tamago324/nlsp-settings.nvim"} -- Language server settings defined in json for
  use {"jose-elias-alvarez/null-ls.nvim"} -- For formatters and linters
  use {"ray-x/lsp_signature.nvim"} -- Show function signature
  use {"j-hui/fidget.nvim"} -- LSP fidget icon at the bottom right
  use {"tami5/lspsaga.nvim"} --  LSP extra functions
  use {"folke/lsp-colors.nvim"} -- Colourise the gutter LSP icons
  use {"onsails/lspkind.nvim"} -- Icons for LSP suggestions 
  use {"kosayoda/nvim-lightbulb"} --  Lightbulb for LSP actions
  use {"RishabhRD/nvim-lsputils"} --  LSP actions user friendly
  use {"nvim-lua/lsp_extensions.nvim"} -- LSP extensions
  use {"Maan2003/lsp_lines.nvim"} -- Lsp in lines

  --  Telescope --
  ------------------
  use {"nvim-telescope/telescope.nvim"}
  use {"ahmedkhalf/project.nvim"} -- Projects list based on VCS
  use {"nvim-telescope/telescope-frecency.nvim"} -- Get results based on weight
	use {"kishikaisei/telescope-js-package-scripts.nvim"} -- Get package.json scripts
  use {"nvim-telescope/telescope-ui-select.nvim"} -- Exposes telescope select
  use {"nvim-telescope/telescope-fzy-native.nvim"} -- FZY sorter

  -- 侮 Treesitter --
  ------------------
  use {"nvim-treesitter/nvim-treesitter", run = ":TSUpdate"} -- Better code detection
  use {"romgrk/nvim-treesitter-context"} -- Better code detection
  use {"JoosepAlviste/nvim-ts-context-commentstring"} -- Comment out code using TS
  use {"nvim-treesitter/nvim-treesitter-angular"} -- TS for Angular

  --  Git --
  -----------
  use {"lewis6991/gitsigns.nvim"}
  -- use {"f-person/git-blame.nvim"}

  --  Colorschemes --
  --------------------
  use {"NTBBloodbath/doom-one.nvim"}
  use {"andersevenrud/nordic.nvim"}
  use {"fenetikm/falcon"}
  use {"folke/tokyonight.nvim"}
  use {"kdheepak/monochrome.nvim"}
  use {"kvrohit/rasmus.nvim"}
  use {"kvrohit/substrata.nvim"}
  use {"lalitmee/cobalt2.nvim"}
  use {"morhetz/gruvbox"}
  use {"navarasu/onedark.nvim"}
  use {"rafamadriz/neon"}
  use {"ray-x/aurora"}
  use {"rebelot/kanagawa.nvim"}
  use {"rmehri01/onenord.nvim"}
  use {"savq/melange"}
  use {"shaunsingh/nord.nvim"}
  use {"tanvirtin/monokai.nvim"}
  use {"theniceboy/nvim-deus"}
  use {"yashguptaz/calvera-dark.nvim"}

  -- Automatically set up your configuration after cloning packer.nvim
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)

